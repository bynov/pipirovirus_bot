package main

import (
	"os"

	"github.com/Syfaro/telegram-bot-api"
)

var token string

const (
	groupID = -1001221056243
)

func init() {
	if v, ok := os.LookupEnv("PIPIROVIRUS_BOT_TOKEN"); ok {
		token = v
	} else {
		panic("PIPIROVIRUS_BOT_TOKEN is not provided")
	}
}

func main() {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		panic(err)
	}

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		panic(err)
	}

	for update := range updates {
		if update.Message.IsCommand() {
			switch update.Message.Command() {
			case "start":
				bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Кукусики"))
				continue
			}
		} else {
			if update.Message.ReplyToMessage != nil {
				bot.Send(tgbotapi.NewMessage(int64(update.Message.ReplyToMessage.ForwardFrom.ID), update.Message.Text))
				continue
			}

			bot.Send(tgbotapi.NewForward(groupID, update.Message.Chat.ID, update.Message.MessageID))
			continue
		}
	}
}
